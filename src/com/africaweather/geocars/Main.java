package com.africaweather.geocars;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.josql.Query;

import com.africaweather.geocars.model.Car;
import com.africaweather.geocars.model.Square;
import com.africaweather.geocars.model.Square.SquarePK;
import com.africaweather.geocars.persistence.CarFactory;
import com.africaweather.model.Storm;
import com.africaweather.persistence.StormDAO;

public class Main {
	public static void main(String[] args) throws Exception {
		Square[][] grid = Square.grid();
		List<Car> cars = CarFactory.getCars(grid, "Cars2.csv");
		StormDAO sdao = new StormDAO();
		System.out.println("Number of cars : " + cars.size());
		List<String> timestamps = sdao.between("2013-12-01 00:00", "2014-01-01 00:00");
		
		long totalHits = 0;
		long max_duration = 0;
		
		for(String timestamp : timestamps) {
			List<Storm> storms = sdao.find(timestamp);
			Query q = new Query();
			q.parse("select * from com.africaweather.model.Storm where reflectance >= 60 and forecast < 60");
			List<Storm> result = (List<Storm>)q.execute(storms).getResults();
			
			System.out.println("Number of storms : " + result.size());
			System.out.println(new Date());
			
			for(int i = 0; i < 16; i++) {
				for(int j = 0; j < 12; j++) {
					if(grid[i][j] != null) grid[i][j].storms = new ArrayList<Storm>();
				}
			}
	
			Map<SquarePK, Square> squareList = new HashMap<Square.SquarePK, Square>();
			
			for(Storm storm : result) {
				for(int i = 0; i < 16; i++) {
					for(int j = 0; j < 12; j++) {
						Square s = grid[i][j];
						if(s != null && storm.shape.intersects(s.area)) {
							s.storms.add(storm);
							squareList.put(new SquarePK(s), s);
						}
					}
				}
			}
			
			long hit = 0;
			Date start = new Date();
			System.out.println("----------");
			System.out.println("Processing : " + timestamp);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date filetime = df.parse(timestamp);
			
			for(Square square : squareList.values()) {
				System.out.print(String.format("Number of cars for Square(%d, %d) : %d [", square.x, square.y, square.cars.size()));
				for(Car car : square.cars) {
					stormLoop : for(Storm s : square.storms) {
						if(s.shape.intersects(CarFactory.safetyZone(car.latitude, car.longitude, 10000))) {
							if(car.lastnotif == null || (filetime.getTime() - car.lastnotif.getTime() > 2*3600*1000)) {
								if(++hit % 1000 == 0) System.out.print(".");
								car.lastnotif = filetime;
								break stormLoop;
							}
						}
					}
				}
				System.out.println("] = " + hit + " hits");
			}
			System.out.println(start);
			System.out.println(new Date());
			long duration = new Date().getTime() - start.getTime();
			System.out.println(duration + "ms");
			if(duration > max_duration) max_duration = duration;
			System.out.println("----------");
			totalHits += hit;
		}

		System.out.println("Work done !");
		System.out.println("Max duration : " + max_duration/1000 + " seconds");
		System.out.println("Total notifications : " + totalHits);
	} 
}
