package com.africaweather.geocars.model;

import java.util.ArrayList;
import java.util.List;

import com.africaweather.model.Storm;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;

public class Square {
	public static class SquarePK {
		public int x;
		public int y;
		
		public SquarePK(Square s) {
			this.x = s.x;
			this.y = s.y;
		}

		public SquarePK(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SquarePK other = (SquarePK) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}
	}
	
	public int x;
	public int y;
	public Geometry area;
	public List<Storm> storms = new ArrayList<Storm>();
	public List<Car> cars = new ArrayList<Car>();
	
	public static WKTReader reader = new WKTReader();
	
	public Square(int x, int y, String wkt) throws Exception {
		this.x = x;
		this.y = y;
		this.area = reader.read(wkt); 
	}
	
	public static Square[] getGrid()  throws Exception {
		int i = 0;
		Square grid[] = new Square[149];
		grid[i++] = new Square(0,5,"POLYGON ((16 -29,17 -29,17 -30,16 -30,16 -29))");
		grid[i++] = new Square(0,6,"POLYGON ((16 -28,17 -28,17 -29,16 -29,16 -28))");
		grid[i++] = new Square(1,1,"POLYGON ((17 -33,18 -33,18 -34,17 -34,17 -33))");
		grid[i++] = new Square(1,2,"POLYGON ((17 -32,18 -32,18 -33,17 -33,17 -32))");
		grid[i++] = new Square(1,3,"POLYGON ((17 -31,18 -31,18 -32,17 -32,17 -31))");
		grid[i++] = new Square(1,4,"POLYGON ((17 -30,18 -30,18 -31,17 -31,17 -30))");
		grid[i++] = new Square(1,5,"POLYGON ((17 -29,18 -29,18 -30,17 -30,17 -29))");
		grid[i++] = new Square(1,6,"POLYGON ((17 -28,18 -28,18 -29,17 -29,17 -28))");
		grid[i++] = new Square(2,0,"POLYGON ((18 -34,19 -34,19 -35,18 -35,18 -34))");
		grid[i++] = new Square(2,1,"POLYGON ((18 -33,19 -33,19 -34,18 -34,18 -33))");
		grid[i++] = new Square(2,2,"POLYGON ((18 -32,19 -32,19 -33,18 -33,18 -32))");
		grid[i++] = new Square(2,3,"POLYGON ((18 -31,19 -31,19 -32,18 -32,18 -31))");
		grid[i++] = new Square(2,4,"POLYGON ((18 -30,19 -30,19 -31,18 -31,18 -30))");
		grid[i++] = new Square(2,5,"POLYGON ((18 -29,19 -29,19 -30,18 -30,18 -29))");
		grid[i++] = new Square(2,6,"POLYGON ((18 -28,19 -28,19 -29,18 -29,18 -28))");
		grid[i++] = new Square(3,0,"POLYGON ((19 -34,20 -34,20 -35,19 -35,19 -34))");
		grid[i++] = new Square(3,1,"POLYGON ((19 -33,20 -33,20 -34,19 -34,19 -33))");
		grid[i++] = new Square(3,2,"POLYGON ((19 -32,20 -32,20 -33,19 -33,19 -32))");
		grid[i++] = new Square(3,3,"POLYGON ((19 -31,20 -31,20 -32,19 -32,19 -31))");
		grid[i++] = new Square(3,4,"POLYGON ((19 -30,20 -30,20 -31,19 -31,19 -30))");
		grid[i++] = new Square(3,5,"POLYGON ((19 -29,20 -29,20 -30,19 -30,19 -29))");
		grid[i++] = new Square(3,6,"POLYGON ((19 -28,20 -28,20 -29,19 -29,19 -28))");
		grid[i++] = new Square(4,0,"POLYGON ((20 -34,21 -34,21 -35,20 -35,20 -34))");
		grid[i++] = new Square(4,1,"POLYGON ((20 -33,21 -33,21 -34,20 -34,20 -33))");
		grid[i++] = new Square(4,2,"POLYGON ((20 -32,21 -32,21 -33,20 -33,20 -32))");
		grid[i++] = new Square(4,3,"POLYGON ((20 -31,21 -31,21 -32,20 -32,20 -31))");
		grid[i++] = new Square(4,4,"POLYGON ((20 -30,21 -30,21 -31,20 -31,20 -30))");
		grid[i++] = new Square(4,5,"POLYGON ((20 -29,21 -29,21 -30,20 -30,20 -29))");
		grid[i++] = new Square(4,6,"POLYGON ((20 -28,21 -28,21 -29,20 -29,20 -28))");
		grid[i++] = new Square(4,7,"POLYGON ((20 -27,21 -27,21 -28,20 -28,20 -27))");
		grid[i++] = new Square(4,8,"POLYGON ((20 -26,21 -26,21 -27,20 -27,20 -26))");
		grid[i++] = new Square(4,9,"POLYGON ((20 -25,21 -25,21 -26,20 -26,20 -25))");
		grid[i++] = new Square(4,10,"POLYGON ((20 -24,21 -24,21 -25,20 -25,20 -24))");
		grid[i++] = new Square(5,0,"POLYGON ((21 -34,22 -34,22 -35,21 -35,21 -34))");
		grid[i++] = new Square(5,1,"POLYGON ((21 -33,22 -33,22 -34,21 -34,21 -33))");
		grid[i++] = new Square(5,2,"POLYGON ((21 -32,22 -32,22 -33,21 -33,21 -32))");
		grid[i++] = new Square(5,3,"POLYGON ((21 -31,22 -31,22 -32,21 -32,21 -31))");
		grid[i++] = new Square(5,4,"POLYGON ((21 -30,22 -30,22 -31,21 -31,21 -30))");
		grid[i++] = new Square(5,5,"POLYGON ((21 -29,22 -29,22 -30,21 -30,21 -29))");
		grid[i++] = new Square(5,6,"POLYGON ((21 -28,22 -28,22 -29,21 -29,21 -28))");
		grid[i++] = new Square(5,7,"POLYGON ((21 -27,22 -27,22 -28,21 -28,21 -27))");
		grid[i++] = new Square(5,8,"POLYGON ((21 -26,22 -26,22 -27,21 -27,21 -26))");
		grid[i++] = new Square(6,0,"POLYGON ((22 -34,23 -34,23 -35,22 -35,22 -34))");
		grid[i++] = new Square(6,1,"POLYGON ((22 -33,23 -33,23 -34,22 -34,22 -33))");
		grid[i++] = new Square(6,2,"POLYGON ((22 -32,23 -32,23 -33,22 -33,22 -32))");
		grid[i++] = new Square(6,3,"POLYGON ((22 -31,23 -31,23 -32,22 -32,22 -31))");
		grid[i++] = new Square(6,4,"POLYGON ((22 -30,23 -30,23 -31,22 -31,22 -30))");
		grid[i++] = new Square(6,5,"POLYGON ((22 -29,23 -29,23 -30,22 -30,22 -29))");
		grid[i++] = new Square(6,6,"POLYGON ((22 -28,23 -28,23 -29,22 -29,22 -28))");
		grid[i++] = new Square(6,7,"POLYGON ((22 -27,23 -27,23 -28,22 -28,22 -27))");
		grid[i++] = new Square(6,8,"POLYGON ((22 -26,23 -26,23 -27,22 -27,22 -26))");
		grid[i++] = new Square(6,9,"POLYGON ((22 -25,23 -25,23 -26,22 -26,22 -25))");
		grid[i++] = new Square(7,0,"POLYGON ((23 -34,24 -34,24 -35,23 -35,23 -34))");
		grid[i++] = new Square(7,1,"POLYGON ((23 -33,24 -33,24 -34,23 -34,23 -33))");
		grid[i++] = new Square(7,2,"POLYGON ((23 -32,24 -32,24 -33,23 -33,23 -32))");
		grid[i++] = new Square(7,3,"POLYGON ((23 -31,24 -31,24 -32,23 -32,23 -31))");
		grid[i++] = new Square(7,4,"POLYGON ((23 -30,24 -30,24 -31,23 -31,23 -30))");
		grid[i++] = new Square(7,5,"POLYGON ((23 -29,24 -29,24 -30,23 -30,23 -29))");
		grid[i++] = new Square(7,6,"POLYGON ((23 -28,24 -28,24 -29,23 -29,23 -28))");
		grid[i++] = new Square(7,7,"POLYGON ((23 -27,24 -27,24 -28,23 -28,23 -27))");
		grid[i++] = new Square(7,8,"POLYGON ((23 -26,24 -26,24 -27,23 -27,23 -26))");
		grid[i++] = new Square(7,9,"POLYGON ((23 -25,24 -25,24 -26,23 -26,23 -25))");
		grid[i++] = new Square(8,0,"POLYGON ((24 -34,25 -34,25 -35,24 -35,24 -34))");
		grid[i++] = new Square(8,1,"POLYGON ((24 -33,25 -33,25 -34,24 -34,24 -33))");
		grid[i++] = new Square(8,2,"POLYGON ((24 -32,25 -32,25 -33,24 -33,24 -32))");
		grid[i++] = new Square(8,3,"POLYGON ((24 -31,25 -31,25 -32,24 -32,24 -31))");
		grid[i++] = new Square(8,4,"POLYGON ((24 -30,25 -30,25 -31,24 -31,24 -30))");
		grid[i++] = new Square(8,5,"POLYGON ((24 -29,25 -29,25 -30,24 -30,24 -29))");
		grid[i++] = new Square(8,6,"POLYGON ((24 -28,25 -28,25 -29,24 -29,24 -28))");
		grid[i++] = new Square(8,7,"POLYGON ((24 -27,25 -27,25 -28,24 -28,24 -27))");
		grid[i++] = new Square(8,8,"POLYGON ((24 -26,25 -26,25 -27,24 -27,24 -26))");
		grid[i++] = new Square(8,9,"POLYGON ((24 -25,25 -25,25 -26,24 -26,24 -25))");
		grid[i++] = new Square(9,0,"POLYGON ((25 -34,26 -34,26 -35,25 -35,25 -34))");
		grid[i++] = new Square(9,1,"POLYGON ((25 -33,26 -33,26 -34,25 -34,25 -33))");
		grid[i++] = new Square(9,2,"POLYGON ((25 -32,26 -32,26 -33,25 -33,25 -32))");
		grid[i++] = new Square(9,3,"POLYGON ((25 -31,26 -31,26 -32,25 -32,25 -31))");
		grid[i++] = new Square(9,4,"POLYGON ((25 -30,26 -30,26 -31,25 -31,25 -30))");
		grid[i++] = new Square(9,5,"POLYGON ((25 -29,26 -29,26 -30,25 -30,25 -29))");
		grid[i++] = new Square(9,6,"POLYGON ((25 -28,26 -28,26 -29,25 -29,25 -28))");
		grid[i++] = new Square(9,7,"POLYGON ((25 -27,26 -27,26 -28,25 -28,25 -27))");
		grid[i++] = new Square(9,8,"POLYGON ((25 -26,26 -26,26 -27,25 -27,25 -26))");
		grid[i++] = new Square(9,9,"POLYGON ((25 -25,26 -25,26 -26,25 -26,25 -25))");
		grid[i++] = new Square(9,10,"POLYGON ((25 -24,26 -24,26 -25,25 -25,25 -24))");
		grid[i++] = new Square(10,1,"POLYGON ((26 -33,27 -33,27 -34,26 -34,26 -33))");
		grid[i++] = new Square(10,2,"POLYGON ((26 -32,27 -32,27 -33,26 -33,26 -32))");
		grid[i++] = new Square(10,3,"POLYGON ((26 -31,27 -31,27 -32,26 -32,26 -31))");
		grid[i++] = new Square(10,4,"POLYGON ((26 -30,27 -30,27 -31,26 -31,26 -30))");
		grid[i++] = new Square(10,5,"POLYGON ((26 -29,27 -29,27 -30,26 -30,26 -29))");
		grid[i++] = new Square(10,6,"POLYGON ((26 -28,27 -28,27 -29,26 -29,26 -28))");
		grid[i++] = new Square(10,7,"POLYGON ((26 -27,27 -27,27 -28,26 -28,26 -27))");
		grid[i++] = new Square(10,8,"POLYGON ((26 -26,27 -26,27 -27,26 -27,26 -26))");
		grid[i++] = new Square(10,9,"POLYGON ((26 -25,27 -25,27 -26,26 -26,26 -25))");
		grid[i++] = new Square(10,10,"POLYGON ((26 -24,27 -24,27 -25,26 -25,26 -24))");
		grid[i++] = new Square(10,11,"POLYGON ((26 -23,27 -23,27 -24,26 -24,26 -23))");
		grid[i++] = new Square(11,1,"POLYGON ((27 -33,28 -33,28 -34,27 -34,27 -33))");
		grid[i++] = new Square(11,2,"POLYGON ((27 -32,28 -32,28 -33,27 -33,27 -32))");
		grid[i++] = new Square(11,3,"POLYGON ((27 -31,28 -31,28 -32,27 -32,27 -31))");
		grid[i++] = new Square(11,4,"POLYGON ((27 -30,28 -30,28 -31,27 -31,27 -30))");
		grid[i++] = new Square(11,5,"POLYGON ((27 -29,28 -29,28 -30,27 -30,27 -29))");
		grid[i++] = new Square(11,6,"POLYGON ((27 -28,28 -28,28 -29,27 -29,27 -28))");
		grid[i++] = new Square(11,7,"POLYGON ((27 -27,28 -27,28 -28,27 -28,27 -27))");
		grid[i++] = new Square(11,8,"POLYGON ((27 -26,28 -26,28 -27,27 -27,27 -26))");
		grid[i++] = new Square(11,9,"POLYGON ((27 -25,28 -25,28 -26,27 -26,27 -25))");
		grid[i++] = new Square(11,10,"POLYGON ((27 -24,28 -24,28 -25,27 -25,27 -24))");
		grid[i++] = new Square(11,11,"POLYGON ((27 -23,28 -23,28 -24,27 -24,27 -23))");
		grid[i++] = new Square(12,2,"POLYGON ((28 -32,29 -32,29 -33,28 -33,28 -32))");
		grid[i++] = new Square(12,3,"POLYGON ((28 -31,29 -31,29 -32,28 -32,28 -31))");
		grid[i++] = new Square(12,4,"POLYGON ((28 -30,29 -30,29 -31,28 -31,28 -30))");
		grid[i++] = new Square(12,5,"POLYGON ((28 -29,29 -29,29 -30,28 -30,28 -29))");
		grid[i++] = new Square(12,6,"POLYGON ((28 -28,29 -28,29 -29,28 -29,28 -28))");
		grid[i++] = new Square(12,7,"POLYGON ((28 -27,29 -27,29 -28,28 -28,28 -27))");
		grid[i++] = new Square(12,8,"POLYGON ((28 -26,29 -26,29 -27,28 -27,28 -26))");
		grid[i++] = new Square(12,9,"POLYGON ((28 -25,29 -25,29 -26,28 -26,28 -25))");
		grid[i++] = new Square(12,10,"POLYGON ((28 -24,29 -24,29 -25,28 -25,28 -24))");
		grid[i++] = new Square(12,11,"POLYGON ((28 -23,29 -23,29 -24,28 -24,28 -23))");
		grid[i++] = new Square(12,12,"POLYGON ((28 -22,29 -22,29 -23,28 -23,28 -22))");
		grid[i++] = new Square(13,3,"POLYGON ((29 -31,30 -31,30 -32,29 -32,29 -31))");
		grid[i++] = new Square(13,4,"POLYGON ((29 -30,30 -30,30 -31,29 -31,29 -30))");
		grid[i++] = new Square(13,5,"POLYGON ((29 -29,30 -29,30 -30,29 -30,29 -29))");
		grid[i++] = new Square(13,6,"POLYGON ((29 -28,30 -28,30 -29,29 -29,29 -28))");
		grid[i++] = new Square(13,7,"POLYGON ((29 -27,30 -27,30 -28,29 -28,29 -27))");
		grid[i++] = new Square(13,8,"POLYGON ((29 -26,30 -26,30 -27,29 -27,29 -26))");
		grid[i++] = new Square(13,9,"POLYGON ((29 -25,30 -25,30 -26,29 -26,29 -25))");
		grid[i++] = new Square(13,10,"POLYGON ((29 -24,30 -24,30 -25,29 -25,29 -24))");
		grid[i++] = new Square(13,11,"POLYGON ((29 -23,30 -23,30 -24,29 -24,29 -23))");
		grid[i++] = new Square(13,12,"POLYGON ((29 -22,30 -22,30 -23,29 -23,29 -22))");
		grid[i++] = new Square(14,3,"POLYGON ((30 -31,31 -31,31 -32,30 -32,30 -31))");
		grid[i++] = new Square(14,4,"POLYGON ((30 -30,31 -30,31 -31,30 -31,30 -30))");
		grid[i++] = new Square(14,5,"POLYGON ((30 -29,31 -29,31 -30,30 -30,30 -29))");
		grid[i++] = new Square(14,6,"POLYGON ((30 -28,31 -28,31 -29,30 -29,30 -28))");
		grid[i++] = new Square(14,7,"POLYGON ((30 -27,31 -27,31 -28,30 -28,30 -27))");
		grid[i++] = new Square(14,8,"POLYGON ((30 -26,31 -26,31 -27,30 -27,30 -26))");
		grid[i++] = new Square(14,9,"POLYGON ((30 -25,31 -25,31 -26,30 -26,30 -25))");
		grid[i++] = new Square(14,10,"POLYGON ((30 -24,31 -24,31 -25,30 -25,30 -24))");
		grid[i++] = new Square(14,11,"POLYGON ((30 -23,31 -23,31 -24,30 -24,30 -23))");
		grid[i++] = new Square(14,12,"POLYGON ((30 -22,31 -22,31 -23,30 -23,30 -22))");
		grid[i++] = new Square(15,5,"POLYGON ((31 -29,32 -29,32 -30,31 -30,31 -29))");
		grid[i++] = new Square(15,6,"POLYGON ((31 -28,32 -28,32 -29,31 -29,31 -28))");
		grid[i++] = new Square(15,7,"POLYGON ((31 -27,32 -27,32 -28,31 -28,31 -27))");
		grid[i++] = new Square(15,8,"POLYGON ((31 -26,32 -26,32 -27,31 -27,31 -26))");
		grid[i++] = new Square(15,9,"POLYGON ((31 -25,32 -25,32 -26,31 -26,31 -25))");
		grid[i++] = new Square(15,10,"POLYGON ((31 -24,32 -24,32 -25,31 -25,31 -24))");
		grid[i++] = new Square(15,11,"POLYGON ((31 -23,32 -23,32 -24,31 -24,31 -23))");
		grid[i++] = new Square(15,12,"POLYGON ((31 -22,32 -22,32 -23,31 -23,31 -22))");
		grid[i++] = new Square(16,6,"POLYGON ((32 -28,33 -28,33 -29,32 -29,32 -28))");
		grid[i++] = new Square(16,7,"POLYGON ((32 -27,33 -27,33 -28,32 -28,32 -27))");
		grid[i++] = new Square(16,8,"POLYGON ((32 -26,33 -26,33 -27,32 -27,32 -26))");
		grid[i++] = new Square(16,9,"POLYGON ((32 -25,33 -25,33 -26,32 -26,32 -25))");
		grid[i++] = new Square(16,10,"POLYGON ((32 -24,33 -24,33 -25,32 -25,32 -24))");
		
		return grid;
	}
	
	public static Square[][] grid() throws Exception {
		Square[][] retval = new Square[17][13];
		for(Square s : getGrid()) {
			retval[s.x][s.y] = s;
		}
		return retval;
	}
}
