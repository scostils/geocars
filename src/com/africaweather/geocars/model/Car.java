package com.africaweather.geocars.model;

import java.util.Date;

public class Car {
	public int 	  customer;
	public double latitude;
	public double longitude;
	
	public int x;
	public int y;
	
	public double xpart;
	public double ypart;
	
	public Date lastnotif;
	
	public Car(){}
	public Car(String line) {
		String[] data = line.split(",");
		this.customer  = Integer.valueOf(data[0]);
		this.latitude  = Double.valueOf(data[1]);
		this.longitude = Double.valueOf(data[2]);
		
		this.x = (int)(Math.ceil(this.longitude) - 17);
		this.y = (int)(Math.ceil(this.latitude) + 34);
		
		long ipart = (long) Math.ceil(this.longitude);
		this.xpart = this.longitude - ipart;
		
		ipart = (long) Math.ceil(Math.abs(this.latitude));
		this.ypart = this.latitude - ipart;
	}
}
