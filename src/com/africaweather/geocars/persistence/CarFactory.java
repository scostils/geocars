package com.africaweather.geocars.persistence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.africaweather.geocars.model.Car;
import com.africaweather.geocars.model.Square;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

public class CarFactory {
	static double border = 0.2;
	
	static CoordinateReferenceSystem pointCRS;
	static CoordinateReferenceSystem radiusCRS;
	static MathTransform transformToUtm;
	static 	MathTransform transformToGeo;
	static GeometryFactory factory = new GeometryFactory(new PrecisionModel(),4326);
	static {
		try {
			radiusCRS = CRS.parseWKT("PROJCS[\"SACRS_NO_27\",GEOGCS[\"WGS 84\",DATUM[\"unknown\",SPHEROID[\"WGS84\",6378137,298.257223563],TOWGS84[0,0,0,0,0,0,0]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",27],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",0],PARAMETER[\"false_northing\",0],UNIT[\"Meter\",1]]");
			pointCRS = CRS.decode("EPSG:4326", true);
			transformToUtm = CRS.findMathTransform(pointCRS, radiusCRS);
			transformToGeo = CRS.findMathTransform(radiusCRS, pointCRS);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	public static Geometry safetyZone(double latitude, double longitude, int radius) throws Exception {
		Point geom = factory.createPoint(new Coordinate(longitude, latitude));
		Geometry targetGeometry = JTS.transform(geom, transformToUtm);
		Geometry buffer = targetGeometry.buffer(radius);
		Geometry bufferGeo = JTS.transform(buffer, transformToGeo);
		return bufferGeo;
	}
	
	public static List<Car> getCars(Square[][] grid, String filename) throws Exception {
		List<Car> retval = new ArrayList<Car>();
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line;
		while((line = reader.readLine()) != null) {
			Car car = new Car(line);
			if(car.x >= 0 && car.x < grid.length && car.y >= 0 && car.y < grid[0].length) {
				if(grid[car.x][car.y] != null) grid[car.x][car.y].cars.add(car);
			}
			retval.add(car);
		}
		reader.close();
		return retval;
	}
}
